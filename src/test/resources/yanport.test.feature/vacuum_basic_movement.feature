Feature: In this test, the vacuum can move in basic movements a board following the instructions

  Scenario: vacuum moves left
    Given The following instructions
      | A |
    And The following initial Position x:5 y:5
    And the following direction : "W"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:5 y:4
    And the following direction: "W"

  Scenario: vacuum moves right
    Given The following instructions
      | A |
    And The following initial Position x:5 y:5
    And the following direction : "E"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:5 y:6
    And the following direction: "E"

  Scenario: vacuum moves up
    Given The following instructions
      | A |
    And The following initial Position x:5 y:5
    And the following direction : "N"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:4 y:5
    And the following direction: "N"

  Scenario: vacuum moves down
    Given The following instructions
      | A |
    And The following initial Position x:5 y:5
    And the following direction : "S"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:6 y:5
    And the following direction: "S"