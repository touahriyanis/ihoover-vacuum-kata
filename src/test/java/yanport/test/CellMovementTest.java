package yanport.test;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import yanport.test.dao.ExecutableInstructionsDAO;
import yanport.test.dao.ExecuteVacuumInstructionsDAO;
import yanport.test.models.*;
import yanport.test.movement_strategies.MovementContext;
import yanport.test.rotation_strategies.RotationContext;

import java.util.List;

public class CellMovementTest {

    private Vacuum vacuum = new Vacuum(null,null,null,null);
    private ExecutableInstructionsDAO service = new ExecuteVacuumInstructionsDAO(new MovementContext(), new RotationContext());

    @Given("The following instructions")
    public void theFollowingInstructions(@Transpose DataTable table) {
        vacuum.setInstructions(TestUtils.convertDatatableToInstructions(table));
    }

    @And("The following initial Position x:{int} y:{int}")
    public void theFollowingInitialPositionXY(int x, int y) {
        vacuum.setCurrentPosition(new Coordinates(x,y));
    }

    @And("the following direction : {string}")
    public void theFollowingDirectionD(String direction) {
        vacuum.setDirection(Direction.valueOf(direction));
    }

    @And("the following board dimensions w:{int} l:{int}")
    public void theFollowingBoardDimensionsWL(int width, int length) {
        vacuum.setBoardDimensions(new BoardDimensions(width, length));
    }

    @When("executing the instructions")
    public void executingTheInstructions() {
        service.execute(vacuum);
    }

    @Then("we should get the following final position x:{int} y:{int}")
    public void weShouldGetTheFollowingFinalPositionXY(int x, int y) {
        Assert.assertEquals(vacuum.getCurrentPosition().getXPosition(), x);
        Assert.assertEquals(vacuum.getCurrentPosition().getYPosition(), y);
    }

    @And("the following direction: {string}")
    public void theFollowingOrientationD(String direction) {
        Assert.assertEquals(vacuum.getDirection(), Direction.valueOf(direction));
    }
}
