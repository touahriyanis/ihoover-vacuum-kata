package yanport.test;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.sl.In;
import yanport.test.models.Instruction;

import java.util.LinkedList;
import java.util.List;

public class TestUtils {

    public static List<Instruction> convertDatatableToInstructions(DataTable dataTable) {
        List<String> instructions = dataTable.asList(String.class);
        List<Instruction> insts = new LinkedList<Instruction>();
        for(var a : instructions) {
            insts.add(Instruction.valueOf(a));
        }
        return insts;
    }
}
