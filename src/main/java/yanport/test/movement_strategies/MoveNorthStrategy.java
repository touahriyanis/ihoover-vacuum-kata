package yanport.test.movement_strategies;

import yanport.test.exceptions.OutOfBoardBoundException;
import yanport.test.models.Vacuum;

public class MoveNorthStrategy implements MovementStrategy {
    @Override
    public void move(Vacuum vacuum) {
        if(vacuum.getCurrentPosition().getXPosition() <= 0) {
            throw new OutOfBoardBoundException();
        }

        int currentXPosition = vacuum.getCurrentPosition().getXPosition();

        vacuum.getCurrentPosition().setXPosition(currentXPosition-1);
    }
}
