package yanport.test.movement_strategies;

import yanport.test.exceptions.OutOfBoardBoundException;
import yanport.test.models.Vacuum;

public class MoveSouthStrategy implements MovementStrategy{
    @Override
    public void move(Vacuum vacuum) {
        if(vacuum.getCurrentPosition().getXPosition() >= vacuum.getBoardDimensions().getWidth()-1) {
            throw new OutOfBoardBoundException();
        }

        int currentXPosition = vacuum.getCurrentPosition().getXPosition();

        vacuum.getCurrentPosition().setXPosition(currentXPosition+1);
    }
}
