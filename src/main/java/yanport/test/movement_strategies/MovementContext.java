package yanport.test.movement_strategies;

import org.springframework.stereotype.Service;
import yanport.test.exceptions.OutOfBoardBoundException;
import yanport.test.models.Direction;
import yanport.test.models.Vacuum;

public class MovementContext {

    private MovementStrategy strategy;

    public void executeMovement(Vacuum vacuum) {
        setMovementStrategy(vacuum.getDirection());
        strategy.move(vacuum);
    }

    public void setMovementStrategy(Direction direction) {
        switch (direction) {
            case N:
                strategy = new MoveNorthStrategy();
                break;
            case S:
                strategy = new MoveSouthStrategy();
                break;
            case E:
                strategy = new MoveEastStrategy();
                break;
            case W:
                strategy = new MoveWestStrategy();
                break;
        }
    }
}
