package yanport.test.movement_strategies;

import yanport.test.exceptions.OutOfBoardBoundException;
import yanport.test.models.Vacuum;

public class MoveWestStrategy implements MovementStrategy{
    @Override
    public void move(Vacuum vacuum) {
        if(vacuum.getCurrentPosition().getYPosition() <= 0) {
            throw new OutOfBoardBoundException();
        }

        int currentYPosition = vacuum.getCurrentPosition().getYPosition();

        vacuum.getCurrentPosition().setYPosition(currentYPosition-1);
    }
}
