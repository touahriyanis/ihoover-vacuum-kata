package yanport.test.movement_strategies;

import yanport.test.models.Direction;
import yanport.test.models.Vacuum;

public interface MovementStrategy {
    void move(Vacuum vacuum);
}
