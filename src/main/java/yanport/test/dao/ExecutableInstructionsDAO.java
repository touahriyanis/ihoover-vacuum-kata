package yanport.test.dao;

import yanport.test.models.Vacuum;

public interface ExecutableInstructionsDAO {
    void execute(Vacuum vacuum);
}
