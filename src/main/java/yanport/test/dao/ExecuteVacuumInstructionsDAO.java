package yanport.test.dao;

import yanport.test.models.Instruction;
import yanport.test.models.Vacuum;
import yanport.test.movement_strategies.MovementContext;
import yanport.test.rotation_strategies.RotationContext;


public class ExecuteVacuumInstructionsDAO implements ExecutableInstructionsDAO{

    private final MovementContext movement;
    private final RotationContext rotation;

    public ExecuteVacuumInstructionsDAO(MovementContext movement, RotationContext rotation) {
        this.movement = movement;
        this.rotation = rotation;
    }

    @Override
    public void execute(Vacuum vacuum) {
        try {
            for(var inst : vacuum.getInstructions()) {
                if(inst == Instruction.D || inst == Instruction.G) {
                    rotation.executeRotation(vacuum, inst);
                } else {
                    movement.executeMovement(vacuum);
                }
            }
        } catch(Exception e) {

        }
    }
}
