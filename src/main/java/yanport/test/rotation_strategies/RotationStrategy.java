package yanport.test.rotation_strategies;

import yanport.test.models.Vacuum;

public interface RotationStrategy {
    void rotate(Vacuum vacuum);
}
