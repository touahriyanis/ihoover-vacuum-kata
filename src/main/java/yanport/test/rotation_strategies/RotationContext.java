package yanport.test.rotation_strategies;


import org.springframework.stereotype.Service;
import yanport.test.models.Direction;
import yanport.test.models.Instruction;
import yanport.test.models.Vacuum;

public class RotationContext {
    private RotationStrategy strategy;

    public void executeRotation(Vacuum vacuum, Instruction instruction) {
        setRotationStrategy(instruction);
        strategy.rotate(vacuum);
    }

    public void setRotationStrategy(Instruction instruction) {
        switch (instruction) {
            case G:
                strategy = new RotateLeftStrategy();
                break;
            case D:
                strategy = new RotateRightStrategy();
                break;
        }
    }
}
