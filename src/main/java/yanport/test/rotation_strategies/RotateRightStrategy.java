package yanport.test.rotation_strategies;

import yanport.test.models.Direction;
import yanport.test.models.Vacuum;

public class RotateRightStrategy  implements RotationStrategy {

    @Override
    public void rotate(Vacuum vacuum) {
        switch (vacuum.getDirection()) {
            case N:
                vacuum.setDirection(Direction.E);
                break;
            case S:
                vacuum.setDirection(Direction.W);
                break;
            case E:
                vacuum.setDirection(Direction.S);
                break;
            case W:
                vacuum.setDirection(Direction.N);
                break;
        }
    }
}
