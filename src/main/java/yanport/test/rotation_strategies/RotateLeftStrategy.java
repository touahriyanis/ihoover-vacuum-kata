package yanport.test.rotation_strategies;

import yanport.test.models.Direction;
import yanport.test.models.Vacuum;

public class RotateLeftStrategy implements RotationStrategy{
    @Override
    public void rotate(Vacuum vacuum) {
        switch (vacuum.getDirection()) {
            case N:
                vacuum.setDirection(Direction.W);
                break;
            case S:
                vacuum.setDirection(Direction.E);
                break;
            case E:
                vacuum.setDirection(Direction.N);
                break;
            case W:
                vacuum.setDirection(Direction.S);
                break;
        }
    }
}
