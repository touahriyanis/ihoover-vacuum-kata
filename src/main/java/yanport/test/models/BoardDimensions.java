package yanport.test.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BoardDimensions {
    private int width;
    private int length;
}
