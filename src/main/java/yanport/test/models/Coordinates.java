package yanport.test.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Coordinates {
    private int xPosition;
    private int yPosition;
}
