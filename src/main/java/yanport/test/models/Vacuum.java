package yanport.test.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Vacuum {
    private Coordinates currentPosition;
    private Direction direction;
    private BoardDimensions boardDimensions;
    private List<Instruction> instructions;
}
