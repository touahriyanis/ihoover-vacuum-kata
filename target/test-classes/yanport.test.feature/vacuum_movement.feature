Feature: In this test, the vacuum can move in a board following the instructions

  Scenario: vacuum moves left
    Given The following instructions
      | D | A | D | A  | D | A | D | A | A |
    And The following initial Position x:5 y:5
    And the following direction : "N"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:4 y:5
    And the following direction: "N"