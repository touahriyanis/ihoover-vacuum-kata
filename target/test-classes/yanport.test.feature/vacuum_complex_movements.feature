Feature: In this test, the vacuum can move in complex movements a board following the instructions

  Scenario: vacuum first complex movement
    Given The following instructions
      | D | A | D | A  | D | A | D | A | A |
    And The following initial Position x:5 y:5
    And the following direction : "N"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:4 y:5
    And the following direction: "N"

  Scenario: vacuum first complex movement
    Given The following instructions
      | D | A | A | D | A  | G | A | D | G | A |
    And The following initial Position x:5 y:5
    And the following direction : "S"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:4 y:1
    And the following direction: "W"

  Scenario: vacuum first complex movement
    Given The following instructions
      | D | G | D | A  | D | A | A | G | A |
    And The following initial Position x:5 y:5
    And the following direction : "W"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:3 y:7
    And the following direction: "N"

  Scenario: vacuum first complex movement
    Given The following instructions
      | G | A | G | A  | D | A | D | D | A |
    And The following initial Position x:5 y:5
    And the following direction : "E"
    And the following board dimensions w:10 l:10
    When executing the instructions
    Then we should get the following final position x:4 y:4
    And the following direction: "S"
